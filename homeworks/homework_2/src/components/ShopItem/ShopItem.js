import React, { PureComponent } from 'react';
import PropTypes from "prop-types";

import "./ShopItem.scss";

class ShopItem extends PureComponent {
	static propTypes = {
		image: PropTypes.string,
		name: PropTypes.string,
		price: PropTypes.number,
		item: PropTypes.number,
		color: PropTypes.string,
	} 

	static defaultProps = {
		image: "https://748073e22e8db794416a-cc51ef6b37841580002827d4d94d19b6.ssl.cf3.rackcdn.com/not-found.png",
		name: "No name",
		price: 0,
		item: "000000",
		color: "black",
	}

	render() {
		const { image, name, price, item, color } = this.props;
		return (
			<div className="shop-item">
				<div className="shop-item__image">
					<img src={image} alt="Product" /> 
				</div>
				<div className="shop-item__info">
					<div className="shop-item__name"> {name} </div>
					<div className="shop-item__price"> ₴{price} </div>
					<div className="shop-item__item"> {item} </div>
					<div className="shop-item__color" style={{backgroundColor: {color}}}/>

				</div>
			</div>			
		)
	}
}

export default ShopItem;